package com.gcash.auth.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvicer {

	@ExceptionHandler(GenerateTokenException.class)
	public ResponseEntity<ErrorResponse> handleGenerateTokenException(GenerateTokenException e) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setCode("GT5XX");
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	@ExceptionHandler(RefreshTokenException.class)
	public ResponseEntity<ErrorResponse> handleRefreshTokenException(RefreshTokenException e) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setCode("RT5XX");
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
		ErrorResponse response = new ErrorResponse();
		response.setErrors(e.getAllErrors().stream().map(ObjectError::getDefaultMessage).toList());
		response.setCode("TX");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> handleException(Exception e) {
		ErrorResponse response = new ErrorResponse();
		response.setError("Something went wrong. Please try again later.");
		response.setCode("EX01");
		e.printStackTrace();
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}
}
