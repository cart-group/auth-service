package com.gcash.auth.service;

import com.gcash.auth.dto.RefreshToken;
import com.gcash.auth.dto.TokenRequest;
import com.gcash.auth.dto.TokenResponse;
import com.gcash.auth.enums.TokenAppType;

public interface AuthorizationService {

	TokenResponse generateToken(TokenRequest request, TokenAppType type);
	
	TokenResponse refreshToken(RefreshToken request, TokenAppType type);
}
