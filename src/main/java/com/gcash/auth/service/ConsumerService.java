package com.gcash.auth.service;

import com.gcash.auth.dto.ApplyConsumerResponse;
import com.gcash.auth.dto.ConsumerEmailRequest;

public interface ConsumerService {
	ApplyConsumerResponse register(ConsumerEmailRequest request);
}
