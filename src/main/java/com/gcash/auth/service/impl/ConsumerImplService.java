package com.gcash.auth.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.gcash.auth.dto.ApplyConsumerRequest;
import com.gcash.auth.dto.ApplyConsumerResponse;
import com.gcash.auth.dto.ConsumerEmailRequest;
import com.gcash.auth.dto.CreateConsumerRequest;
import com.gcash.auth.dto.CreateConsumerResponse;
import com.gcash.auth.service.ConsumerService;

@Service
public class ConsumerImplService implements ConsumerService {
	
	@Value("${kong.client-id}")
	private String clientId;
	
	@Value("${kong.client-secret}")
	private String clientSecret;
	
	@Value("${kong.consumer.name}")
	private String kongConsumerName;
	
	@Value("${kong.redirect_uris}")
	private String redirectUris;
	
	@Value("${kong.consumer.endpoint}")
	private String kongEndpoint;
	
	@Autowired
	private WebClient webClient;
	

	@Override
	public ApplyConsumerResponse register(ConsumerEmailRequest request) {
		String email = request.getEmail();
		createConsumer(email);
		return applyConsumer(email);
	}
	
	private CreateConsumerResponse createConsumer(String email) {
		String endpoint = UriComponentsBuilder
				.fromHttpUrl(kongEndpoint)
				.path("/consumers")
				.toUriString();
		
		return webClient.post()
				.uri(endpoint)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.body(BodyInserters.fromFormData("username", email)
						.with("custom_id", UUID.randomUUID().toString()))
				.retrieve()
				.bodyToMono(CreateConsumerResponse.class)
				.block();
	}
	
	private ApplyConsumerResponse applyConsumer(String email) {
		ApplyConsumerRequest request = new ApplyConsumerRequest();
		request.setClientId(clientId);
		request.setClientSecret(clientSecret);
		request.setName(kongConsumerName);
		request.setRedirectUris(redirectUris);
		
		MultiValueMap<String, String> requestmap = new LinkedMultiValueMap<>();
		requestmap.set("name", kongConsumerName);
		requestmap.set("redirect_uris", redirectUris);
		
		String endpoint = UriComponentsBuilder
				.fromHttpUrl(kongEndpoint)
				.path("/consumers")
				.path("/")
				.path(email)
				.path("/oauth2")
				.toUriString();
		
		return webClient.post()
				.uri(endpoint)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.body(BodyInserters.fromFormData(requestmap))
				.retrieve()
				.bodyToMono(ApplyConsumerResponse.class)
				.block();
	}

}
