package com.gcash.auth.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import com.gcash.auth.dto.RefreshToken;
import com.gcash.auth.dto.ConsumerEmailRequest;
import com.gcash.auth.dto.GenerateTokenRequest;
import com.gcash.auth.dto.RefreshTokenRequest;
import com.gcash.auth.dto.TokenRequest;
import com.gcash.auth.dto.TokenResponse;
import com.gcash.auth.enums.TokenAppType;
import com.gcash.auth.exceptions.GenerateTokenException;
import com.gcash.auth.exceptions.RefreshTokenException;
import com.gcash.auth.service.AuthorizationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AuthorizationImplService implements AuthorizationService {
	
	@Autowired
	private WebClient webClient;
	
	@Value("${kong.token.endpoint}")
	private String kongEndpoint;
	
	@Value("${kong.product.provision-key}")
	private String productProvisionKey;
	
	@Value("${kong.cart.provision-key}")
	private String cartProvisionKey;
	
	private static final String CART_PATH = "carts";
	
	private static final String PRODUCT_PATH = "products";

	@Override
	public TokenResponse generateToken(TokenRequest request, TokenAppType type) {
		String provisionKey;
		if (type.equals(TokenAppType.CART)) {
			provisionKey = cartProvisionKey;
		} else {
			provisionKey = productProvisionKey;
		}
		
		GenerateTokenRequest tokenRequest = new GenerateTokenRequest();
		tokenRequest.setAuthenticatedUserid(request.getEmail());
		tokenRequest.setClientId(request.getClientId());
		tokenRequest.setClientSecret(request.getClientSecret());
		tokenRequest.setGrantType("password");
		tokenRequest.setScope("read");
		tokenRequest.setProvisionKey(provisionKey);
		
		try {
			return webClient.post()
					.uri(generateEndpoint(type))
					.contentType(MediaType.APPLICATION_JSON)
					.bodyValue(tokenRequest)
					.retrieve()
					.bodyToMono(TokenResponse.class)
					.block();
		} catch(RuntimeException e) {
			log.error("Cannot generate token {}", e.getMessage());
			throw new GenerateTokenException();
		}
	}
	
	@Override
	public TokenResponse refreshToken(RefreshToken request, TokenAppType type) {
		RefreshTokenRequest refreshTokenRequest = new RefreshTokenRequest();
		refreshTokenRequest.setClientId(request.getClientId());
		refreshTokenRequest.setClientSecret(request.getClientSecret());
		refreshTokenRequest.setGrantType("password");
		refreshTokenRequest.setRefreshToken(request.getToken());
		
		try {
			return webClient.post()
					.uri(generateEndpoint(type))
					.contentType(MediaType.APPLICATION_JSON)
					.bodyValue(refreshTokenRequest)
					.retrieve()
					.bodyToMono(TokenResponse.class)
					.block();
		} catch(RuntimeException e) {
			log.error("Cannot refresh token {}", e.getMessage());
			throw new RefreshTokenException();
		}
	}
	
	private String generateEndpoint(TokenAppType type) {
		String path = "";
		if (type.equals(TokenAppType.CART)) {
			path = CART_PATH;
		} else {
			path = PRODUCT_PATH;
		}
		
		return UriComponentsBuilder
				.fromHttpUrl(kongEndpoint)
				.path("/")
				.path(path)
				.path("/oauth2/token")
				.toUriString();
	}
}
