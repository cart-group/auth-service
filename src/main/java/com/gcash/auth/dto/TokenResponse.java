package com.gcash.auth.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Data;

@Data
public class TokenResponse {
	@JsonAlias("access_token")
	private String accessToken;
	
	@JsonAlias("refresh_token")
	private String refreshToken;
	
	@JsonAlias("token_type")
	private String tokenType;
	
	@JsonAlias("expires_in")
	private String expiresIn;
}
