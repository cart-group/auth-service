package com.gcash.auth.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Data;

@Data
public class RefreshTokenRequest {
	@JsonAlias("client_id")
	private String clientId;
	
	@JsonAlias("client_secret")
	private String clientSecret;
	
	@JsonAlias("grant_type")
	private String grantType;
	
	@JsonAlias("refresh_token")
	private String refreshToken;
}
