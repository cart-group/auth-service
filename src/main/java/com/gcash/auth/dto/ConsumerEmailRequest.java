package com.gcash.auth.dto;

import jakarta.validation.constraints.Email;
import lombok.Data;

@Data
public class ConsumerEmailRequest {

	@Email
	private String email;
}
