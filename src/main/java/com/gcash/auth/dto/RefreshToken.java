package com.gcash.auth.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class RefreshToken {
	
	@NotBlank
	@JsonAlias("refresh_token")
	private String token;
	
	@NotBlank
	private String clientId;
	
	@NotBlank
	private String clientSecret;
}
