package com.gcash.auth.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Data;

@Data
public class CreateConsumerRequest {
	private String username;
	
	@JsonAlias("custom_id")
	private String customId;
}
