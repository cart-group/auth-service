package com.gcash.auth.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class GenerateTokenRequest {
	@JsonProperty("client_id")
	private String clientId;
	
	@JsonProperty("client_secret")
	private String clientSecret;
	
	@JsonProperty("grant_type")
	private String grantType;
	
	@JsonProperty("provision_key")
	private String provisionKey;
	
	@JsonProperty("authenticated_userid")
	private String authenticatedUserid;
	
	private String scope;
}
