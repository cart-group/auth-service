package com.gcash.auth.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class TokenRequest {
	@Email
	private String email;
	
	@NotBlank
	@JsonAlias("client_id")
	private String clientId;
	
	@NotBlank
	@JsonAlias("client_secret")
	private String clientSecret;
}
