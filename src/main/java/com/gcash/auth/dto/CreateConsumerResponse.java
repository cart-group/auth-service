package com.gcash.auth.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateConsumerResponse {
/*
 * {
    "type": 0,
    "created_at": 1683164172,
    "username_lower": "badong4@mailinator.com",
    "username": "badong4@mailinator.com",
    "id": "9607a651-470a-452d-a32d-cc574e4fd2ce",
    "tags": null,
    "custom_id": "100004"
}
 * */
	private String id;
	
	private String username;
	
	@JsonAlias("custom_id")
	private String customId;
}
