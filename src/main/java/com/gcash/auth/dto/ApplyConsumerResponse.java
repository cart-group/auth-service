package com.gcash.auth.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApplyConsumerResponse {
	private String id;
	
	private String name;
	
	@JsonAlias("client_id")
	private String clientId;
	
	@JsonAlias("client_secret")
	private String clientSecret;
}
