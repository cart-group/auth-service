package com.gcash.auth.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Data;

@Data
public class ApplyConsumerRequest {
	private String name;
	
	@JsonAlias("client_id")
	private String clientId;
	
	@JsonAlias("client_secret")
	private String clientSecret;
	
	@JsonAlias("redirect_uris")
	private String redirectUris;
}
