package com.gcash.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcash.auth.dto.ApplyConsumerResponse;
import com.gcash.auth.dto.ConsumerEmailRequest;
import com.gcash.auth.service.ConsumerService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("consumers")
public class ConsumerController {
	
	@Autowired
	private ConsumerService consumerService;
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity<ApplyConsumerResponse> createCustomer(@RequestBody @Valid ConsumerEmailRequest request) {
		ApplyConsumerResponse response = consumerService.register(request);
		return ResponseEntity.accepted().body(response);
	}
	
	@GetMapping
	public ResponseEntity<String> gey() {
		return ResponseEntity.ok("hohohaha");
	}
}
