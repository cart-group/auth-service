package com.gcash.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gcash.auth.dto.ConsumerEmailRequest;
import com.gcash.auth.dto.RefreshToken;
import com.gcash.auth.dto.TokenRequest;
import com.gcash.auth.dto.TokenResponse;
import com.gcash.auth.enums.TokenAppType;
import com.gcash.auth.service.AuthorizationService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("auth/token")
public class AuthorizationController {
	
	@Autowired
	private AuthorizationService authorizationService;

	@PostMapping
	public ResponseEntity<TokenResponse> generateToken(@RequestBody @Valid TokenRequest request, @RequestParam TokenAppType type) {
		return ResponseEntity.ok(authorizationService.generateToken(request, type));
	}
	
	@PostMapping("refresh")
	public ResponseEntity<TokenResponse> refreshToken(@RequestBody @Valid RefreshToken request, @RequestParam TokenAppType type) {
		return ResponseEntity.ok(authorizationService.refreshToken(request, type));
	}
}
